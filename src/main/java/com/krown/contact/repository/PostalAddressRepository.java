package com.krown.contact.repository;

import com.krown.contact.entities.Contact;
import com.krown.contact.entities.PostalAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PostalAddressRepository extends JpaRepository<PostalAddress, Long> {
    @Query("SELECT p FROM PostalAddress p WHERE p.cp = ?1 AND p.first_line = ?2 AND p.second_line = ?3 AND p.ville = ?4")
    public PostalAddress findPostalAdress(String cp, String first_line, String second_line, String ville);
}
