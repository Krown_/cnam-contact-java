package com.krown.contact.repository;

import com.krown.contact.entities.Contact;
import com.krown.contact.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {
    @Query("SELECT u FROM Contact u WHERE u.user = ?1")
    public List<Contact> findContactByUserId(User user);

    @Query("SELECT u FROM Contact u WHERE u.user = ?1 AND u.id = ?2")
    public Contact findContactByUserIdAndContactId(User user, Long contactId);
}
