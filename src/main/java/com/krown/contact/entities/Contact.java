package com.krown.contact.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="contacts")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String phoneNumber;

    @ManyToMany(mappedBy = "contacts", fetch = FetchType.LAZY)
    private Set<PostalAddress> postalAddresses = new HashSet<>();

    @OneToMany(mappedBy = "contact", fetch = FetchType.LAZY)
    private Set<Email> emails = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    public Contact(String _name, String _phoneNumber) {
        this.name = _name;
        this.phoneNumber = _phoneNumber;
    }

    public Contact() {

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean hasMails()
    {
        return !getEmails().isEmpty();
    }

    public boolean hasPostalAddresses()
    {
        return !getPostalAddresses().isEmpty();
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }

    public Set<PostalAddress> getPostalAddresses() {
        return postalAddresses;
    }

    public void setPostalAddresses(Set<PostalAddress> postalAddresses) {
        this.postalAddresses = postalAddresses;
    }

    public void removePostalAddresses(PostalAddress _postalAddress)
    {
        this.postalAddresses.remove(_postalAddress);
        _postalAddress.getContacts().remove(this);
    }


    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", postalAddresses=" + postalAddresses +
                ", emails=" + emails +
                '}';
    }
}
