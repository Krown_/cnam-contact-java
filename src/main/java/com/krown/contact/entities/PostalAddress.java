package com.krown.contact.entities;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="postal_address")
public class PostalAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String ville;
    private String cp;
    private String first_line;
    private String second_line;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "postal_address_contact",
            joinColumns = { @JoinColumn(name = "postal_address_id") },
            inverseJoinColumns = { @JoinColumn(name = "contact_id") })
    private Set<Contact> contacts = new HashSet<>();

    public PostalAddress(String _ville, String _cp, String _first_line, String _second_line)
    {
        this.ville = _ville;
        this.cp = _cp;
        this.first_line = _first_line;
        this.second_line = _second_line;
    }

    public PostalAddress ()
    {

    }

    public Long getId() {
        return id;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getFirstLine() {
        return first_line;
    }

    public void setFirstLine(String first_line) {
        this.first_line = first_line;
    }

    public String getSecondLine() {
        return second_line;
    }

    public void setSecondLine(String second_line) {
        this.second_line = second_line;
    }

    public Set<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Set<Contact> contacts) {
        this.contacts = contacts;
    }

    @Override
    public String toString() {
        return cp + ",  " + ville + ", " + first_line + ", " + second_line;
    }
}
