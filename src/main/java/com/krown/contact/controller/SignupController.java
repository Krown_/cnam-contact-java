package com.krown.contact.controller;

import com.krown.contact.entities.User;
import com.krown.contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SignupController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/signup")
    public String home(Model model)
    {
        model.addAttribute("title", "Contact Manager | Signup");
        model.addAttribute("user", new User());
        return "signup";
    }

    @RequestMapping(value = "/do_signup", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") User user, Model model)
    {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(user.getPassword());

        user.setPassword(encodedPassword);

        userRepository.save(user);

        return "login";
    }
}
