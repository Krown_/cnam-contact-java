package com.krown.contact.controller;

import com.krown.contact.entities.Contact;
import com.krown.contact.entities.Email;
import com.krown.contact.entities.PostalAddress;
import com.krown.contact.entities.User;
import com.krown.contact.repository.ContactRepository;
import com.krown.contact.repository.EmailRepository;
import com.krown.contact.repository.PostalAddressRepository;
import com.krown.contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private PostalAddressRepository postalAddressRepository;

    @Autowired
    private EmailRepository emailRepository;

    @ModelAttribute
    public void addCommonData(Model model, Principal principal)
    {
        String email = principal.getName();

        User user = userRepository.findByEmail(email);

        model.addAttribute("user", user);
    }

    @GetMapping("/addContact")
    public String addContactForm(Model model)
    {
        model.addAttribute("title", "Contact Manager | AddContact");
        model.addAttribute("contact", new Contact());
        return "contact/addContact";
    }

    @GetMapping("/listContact")
    public String listContacts(Model model, Principal principal)
    {
        String emailUser = principal.getName();

        User user = userRepository.findByEmail(emailUser);
        List<Contact> listContacts = contactRepository.findContactByUserId(user);

        model.addAttribute("listContacts", listContacts);

        return "contact/listContact";
    }

    @GetMapping("/contact/edit/{id}")
    public String editContact(Model model, @PathVariable Long id) {

        Optional<Contact> contact = contactRepository.findById(id);
        model.addAttribute("editContact", contact);

        return "contact/editContact";
    }

    @GetMapping("/contact/delete/{id}")
    public String deleteContact(@PathVariable Long id, Principal principal)
    {
        String emailUser = principal.getName();

        User user = userRepository.findByEmail(emailUser);

        Contact contact = contactRepository.findContactByUserIdAndContactId(user, id);

        contact.getPostalAddresses().removeAll(contact.getPostalAddresses());

        for (Email emailObj:contact.getEmails()) {
            emailRepository.deleteById(emailObj.getId());
        }

        contactRepository.delete(contact);

        return "home";
    }

    @GetMapping("/contact/infos/{id}")
    public String infosContact(Model model, @PathVariable Long id) {

        Optional<Contact> contact = contactRepository.findById(id);
        model.addAttribute("infoContact", contact);

        return "contact/infosContact";
    }

    @PostMapping("/add-contact")
    public String processContact(@RequestParam String name,
                                 @RequestParam String phoneNumber,
                                 @RequestParam Integer email_number,
                                 @RequestParam Integer adr_number,
                                 @RequestParam ArrayList<String> email,
                                 @RequestParam ArrayList<String> ville,
                                 @RequestParam ArrayList<String> code_postal,
                                 @RequestParam ArrayList<String> first_line,
                                 @RequestParam ArrayList<String> second_line, Principal principal) {

        String emailUser = principal.getName();

        User user = userRepository.findByEmail(emailUser);

        Contact contact = new Contact(name, phoneNumber);

        contact.setUser(user);

        contactRepository.save(contact);

        if(email_number > 0)
        {
            for (int i = 0; i < email_number; ++i)
            {
                Email emailObj = new Email(email.get(i));
                emailObj.setContact(contact);
                emailRepository.save(emailObj);
            }
        }

        if(adr_number > 0)
        {
            for (int i = 0; i < adr_number; ++i)
            {
                PostalAddress postalAddressObj = new PostalAddress(ville.get(i), code_postal.get(i), first_line.get(i), second_line.get(i));
                postalAddressObj.getContacts().add(contact);
                postalAddressRepository.save(postalAddressObj);
            }
        }

        return "home";
    }

    @Transactional
    @PostMapping("/edit-contact")
    public String editContact(@RequestParam Long contactId,
                                 @RequestParam String name,
                                 @RequestParam String phoneNumber,
                                 @RequestParam Integer email_number,
                                 @RequestParam Integer adr_number,
                                 @RequestParam ArrayList<String> email,
                                 @RequestParam ArrayList<String> ville,
                                 @RequestParam ArrayList<String> code_postal,
                                 @RequestParam ArrayList<String> first_line,
                                 @RequestParam ArrayList<String> second_line, Principal principal) {

        System.out.println("nom : " + name + " numéro : " + phoneNumber + " emails : " +email
                + " villes : " +ville
                + " cp's : " +code_postal
                + " fl's : " +first_line
                + " sl's : " +second_line);


        String emailUser = principal.getName();

        User user = userRepository.findByEmail(emailUser);

        Contact oldContact = contactRepository.findContactByUserIdAndContactId(user, contactId);

        oldContact.getPostalAddresses().removeAll(oldContact.getPostalAddresses());

        for (Email emailObj:oldContact.getEmails()) {
            emailRepository.deleteById(emailObj.getId());
        }

        contactRepository.delete(oldContact);

        Contact contact = new Contact(name, phoneNumber);

        contact.setUser(user);

        contactRepository.save(contact);

        if(email_number > 0)
        {
            for (int i = 0; i < email_number; ++i)
            {
                Email emailObj = new Email(email.get(i));
                emailObj.setContact(contact);
                emailRepository.save(emailObj);
            }
        }

        if(adr_number > 0)
        {
            for (int i = 0; i < adr_number; ++i)
            {
                PostalAddress postalAddressObj = new PostalAddress(ville.get(i), code_postal.get(i), first_line.get(i), second_line.get(i));
                postalAddressObj.getContacts().add(contact);
                postalAddressRepository.save(postalAddressObj);
            }
        }

        return "home";
    }

}
