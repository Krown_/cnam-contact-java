package com.krown.contact.controller;

import com.krown.contact.entities.Contact;
import com.krown.contact.entities.User;
import com.krown.contact.repository.ContactRepository;
import com.krown.contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ContactRepository contactRepository;

    @GetMapping("/")
    public String home(Model model)
    {
        model.addAttribute("title", "Contact Manager | Home");
        return "home";
    }


    @GetMapping("/test")
    @ResponseBody
    public String test()
    {
        User user = new User("Maxance", "test@gmail.com");

        Contact contact = new Contact("test", "0633016485");

        user.getContacts().add(contact);

        contactRepository.save(contact);
        userRepository.save(user);

        return "WIP";
    }
}
