package com.krown.contact.controller;

import com.krown.contact.entities.User;
import com.krown.contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/login")
    public String home(Model model) {
        model.addAttribute("title", "Contact Manager | Login");
        model.addAttribute("user", new User());
        return "login";
    }
}
