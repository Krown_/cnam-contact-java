package com.krown.contact.controller;

import com.krown.contact.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogoutController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/logout")
    public String home(Model model) {
        SecurityContextHolder.getContext().setAuthentication(null);
        return "home";
    }
}
